#!/usr/bin/env python

import argparse

def main():
    """
    This function is defined to be the main entry point for the `fgsignalbot`
    command in setup.py.
    """
    parser = argparse.ArgumentParser(
                    prog='fgsignalbot',
                    description='Signal Bot für Fachschaften/Fachgruppen',
                    )
    parser.add_argument('-t', '--test', action='store_true', help="Test the bot.")

    args = parser.parse_args()

    if args.test:
        print("Testing...")

    print("Hello World")


if __name__ == "__main__":
    """
    This part is not the actual main. This is just useful for testing.
    """
    main()
