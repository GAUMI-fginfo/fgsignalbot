from setuptools import find_packages, setup

setup(
    name='fgsignalbot',
    packages=find_packages(include=['fgsignalbot']),
    version='0.1.0',
    description='Signal Bot für Fachschaften/Fachgruppen',
    author='Maintainer',
    url='https://gitlab.gwdg.de/GAUMI-fginfo/fgsignalbot',
    entry_points = {
        'console_scripts': ['fgsignalbot=fgsignalbot.cli:main'],
    },
    install_requires=[
    ],
)
