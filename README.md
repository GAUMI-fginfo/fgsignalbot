# FGSignalBot

As seen on https://ak.kif.rocks/kif520/submission/ak/1006/.

## Abhängigkeiten

```bash
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```
Alternativ geht auch einfach `make venv` ausführen.

## Entwickeln / Testen

```bash
python3 setup.py install
fgsignalbot --help
fgsignalbot --test
```
Alternativ geht auch einfach `make test` oder `make` ausführen.

## Bauen / Installieren

```bash
python3 setup.py bdist_wheel
```
Alternativ geht auch einfach `make dist` ausführen.

In `dist/` ist dann eine installierbare `.whl` Datei mit `pip3 install <whl Datei>`.

## Links

- https://github.com/AsamK/signal-cli
- https://docs.python.org/3/library/argparse.html
- https://python-packaging.readthedocs.io/en/latest/command-line-scripts.html

