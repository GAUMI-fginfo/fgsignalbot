#!/bin/bash

set -e # Exit on error

pushd "$(dirname "$0")" >/dev/null
mkdir -p deps
pushd deps >/dev/null

# Download signal-cli if missing
# https://github.com/AsamK/signal-cli
export SIGNALCLIVERSION="0.13.11"
export SIGNALCLIDIR="signal-cli-${SIGNALCLIVERSION}"
if [ ! -d "${SIGNALCLIDIR}" ] || [ "$(./signal-cli/bin/signal-cli --version)" != "signal-cli ${SIGNALCLIVERSION}" ]; then
	echo "Downloadng signal-cli v${SIGNALCLIVERSION}"
	wget "https://github.com/AsamK/signal-cli/releases/download/v${SIGNALCLIVERSION}/signal-cli-${SIGNALCLIVERSION}.tar.gz" -O signal-cli.tar.gz
	wget "https://github.com/AsamK/signal-cli/releases/download/v${SIGNALCLIVERSION}/signal-cli-${SIGNALCLIVERSION}.tar.gz.asc" -O signal-cli.tar.gz.asc

	# verify signature
	gpg --recv-keys FA10826A74907F9EC6BBB7FC2BA2CD21B5B09570  # download and import public key: AsamK <asamk@gmx.de>
	gpg --verify signal-cli.tar.gz.asc signal-cli.tar.gz  # verify signature

	# tar xf signal-cli.tar.gz -C /opt
	# ln -sf /opt/signal-cli-"${VERSION}"/bin/signal-cli /usr/local/bin/
	tar xf signal-cli.tar.gz
	rm -f signal-cli.tar.gz
	rm -f signal-cli.tar.gz.asc
	ln -sf "${SIGNALCLIDIR}" "./signal-cli"
	[ "$(./signal-cli/bin/signal-cli --version)" == "signal-cli ${SIGNALCLIVERSION}" ]  # verify version
	echo "signal-cli v${SIGNALCLIVERSION} downloaded."
else
	echo "signal-cli is already v${SIGNALCLIVERSION}"
fi

popd >/dev/null
popd >/dev/null
