.PHONY: default
default: test

.PHONY: test
test: setup venv install
	. venv/bin/activate && \
		fgsignalbot --test

.PHONY: install
install: setup venv
	. venv/bin/activate && \
		python3 setup.py install

.PHONY: dist
dist: setup venv
	. venv/bin/activate && \
		python3 setup.py bdist_wheel

.PHONY: setup
setup:
	./setup.sh

venv: requirements.txt
	python3 -m venv venv
	. venv/bin/activate && \
		pip3 install -r requirements.txt

